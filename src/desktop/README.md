# Ansible Playbook per la configurazione di laptop e/o pc

# Software

- ppa
- telegram
- chrome
- vscode
- ssh

# Configurazioni

- apt
- gnome
  - antialiasing
  - clock-format
- firewall
  - porte 22,80
